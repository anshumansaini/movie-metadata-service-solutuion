import os
import sys
import json
import glob
import traceback

print ("Please note all the search are case insensitive only for search fields not for Category. Thank You!")


print ("Please enter the desired search field movie search : ")
mcategory = input()

print ("Please enter the desired input for given search field : ")
msearch = input()


path = sys.argv[1] # To pass the folder location with all json repositories as an argument. 

def search_data():
	'''
	This function will check for all the json repositories we have created by merging two datsets.
	It will provide us with the final output i.e. the Array of jsons as per the user input.
	'''
	try:
		files = glob.glob(path+'*.json')

		alljsons = []

		for fils in files:
			f = open(fils,'rb')
			data = f.read()
			f.close()

			fjson = json.loads(data)

			#To check for the desired condition.
			if msearch.lower() in str(fjson[mcategory]).lower():
				print ("The desired result is in the file :",fils)
				alljsons.append(fjson)
				print (fjson)
	except:
		traceback.print_exc()

		print ("The output for the desired query is : "+'\n',alljsons)

if __name__ == "__main__":
	search_data()




