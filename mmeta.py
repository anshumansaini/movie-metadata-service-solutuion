import requests
import json
import glob, sys
import traceback

print ("Please enter the desired IMBD id to search : ")
id = input()

def meta_data():
	'''
	This function will fread all the json files in the specified location.
	'''
	try:
		path = sys.argv[1] # To pass the folder location where all json files are loacted as an argument. 

		files = glob.glob(path+'*.json')

		json_obj = []

		for fils in files:
			f = open(fils,'rb')
			data = f.read()
			f.close()

			json_obj.append(data)

		for data in json_obj:
			rdata = json.loads(data)

			if rdata['imdbId']==id:
			
				json2 = rdata
	except:
		traceback.print_exc()

	return json2



def ombd_data():
	'''
	It is a function to retrieve Metadata from the OMDB API and return a json object.
	'''
	try:
		response = requests.get('http://www.omdbapi.com/?i='+id+'&apikey=68fd98ab&plot=full')
		json1 = response.json()

	except:
		traceback.print_exc()

	return json1



def compare_json(json1,json2):
	'''
	This function will compare the two jsons.
	Modify the json objects and merge them into a desired output json file.
	As per the instructions given in the assignment.
	'''

	try:
		json1['Title']=json2['title']
		json1['Plot']=json2['description']
		json1['Runtime']=json2['duration']
		json1['Ratings'].append(json2['userrating'])
		json1['String'] = []

		json1['String'].extend([json1['Director'],json1['Writer'],json1['Actors']])

		diff = list(set(json2) - set(json1) - {'languages', 'userrating', 'imdbId', 'description', 'title', 'duration'})

		for lp in diff:
			try:
				json1[lp] = json2[lp]
			except:
				pass
		filenam = json1['imdbID']
		f1 = open(sys.argv[2]+filenam+'.json','w')
		json.dump(json1,f1)
		f1.close()
	except:
		traceback.print_exc()

if __name__ == "__main__":

	json1 = ombd_data()
	json2 = meta_data()
	compare_json(json1,json2)

	print ("The desired output file is created at ./mergedjson folder. Thank You!")
